# Possible causes of climate change

* Which natural features correlate with climate changes?
* Which human-made features correlate with climate changes?
* In which countries are those features produced most?
* What is the most important feature correlated to climate change?
