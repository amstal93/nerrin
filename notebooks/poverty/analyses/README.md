# Poverty analyses

## 2017
* [MPI National dataset correlations](correlations_nat.ipynb)
* [MPI Subnational dataset correlations](correlations_subnat.ipynb)
* [Analysis of poverty in world regions 2017](world_regions_2017.ipynb)
* [Principal component analysis country HELP dataset](principal_components.ipynb)
* [KMeans clustering of the country HELP dataset](kmeans.ipynb)
* [Regression on country HELP dataset](regression.ipynb)
* [Subclustering of clusters 0 and 1 with MPI included](subclustering.ipynb)

## 2021