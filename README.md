# Nerrin

These notebooks serve us to inform ourselves and practice some data analysis and visualisation techniques.

* [Exploratory Data Analysis (EDA) Titanic](notebooks/titanic)
* [Poverty analysis](notebooks/poverty)
* [Climate change predictions](notebooks/climate-change)

...

## Requirements
Anaconda3 with python 3.9 interpreter

## Installation

### Docker

Download docker from dockerhub, or build from the Dockerfile, in the repo root directory:

```bash
$ docker build -t nerrin .
```

Run with:

```bash
$ docker run -p 8888:8888 -v $(pwd):/nerrin nerrin
```
This will give access to jupyter in browser. To enter the cli (in another terminal with the container is running):

```bash
$ docker run -it -v $(pwd):/nerrin nerrin /bin/bash
```

### Repository
Fork or download, [install anaconda](https://www.anaconda.com/products/individual), and in conda virtual environment for the project, run:

```bash
$ conda env create -f environment.yml
```

After updating packages (and subsequently running the tests):

```bash
$ conda env export > environment.yml --no-builds
```

And the locked dependency list:

```bash
$ conda env export > environment.lock.yml
```

## Code quality

We automated the code formatting with [black-jupyter](https://anaconda.org/conda-forge/black-jupyter) and PEP8 compliance with [flake8_nb](https://anaconda.org/conda-forge/flake8-nb) processes
by using the [pre-commit framework](https://anaconda.org/conda-forge/pre_commit) and its [hooks](https://anaconda.org/conda-forge/pre-commit-hooks).
It runs a short script before committing. If the script passes, then the commit is made, else, the commit is denied.

Running Black Jupyter on all notebooks from the root of the repo:

```bash
$ black .
```

Running Flake8 NB on all notebooks from the root of the repo:

```bash
$ flake8_nb notebooks
```

If you wish to additionally use type annotations and [mypy](https://anaconda.org/anaconda/mypy), install [data-science-types](https://anaconda.org/conda-forge/data-science-types) for libraries like matplotlib, numpy and pandas that do not have type information, and install [nbqa](https://anaconda.org/conda-forge/nbqa).

Run on all notebooks in the root directory of the repo with:

```bash
$ nbqa mypy .
```

Run on a specific `notebook.ipynb` with:

```bash
$ nbqa mypy notebook.ipynb
```

## Testing

To see the [test coverage report](https://anaconda.org/anaconda/pytest-cov):
```bash
$ pytest --cov
```
To get a durations report:
```bash
$ pytest --durations=3
```
To run end-to-end testing:
```bash
$ pytest --nbmake
```
To speed things up with xdist:
```bash
$ pytest --nbmake -n=auto
```

## Security

Check installed dependencies for known security vulnerabilities:

```bash
$ safety check
```

Check Conda packages (python and system):

```bash
$ conda list | jake ddt -c | grep VULNERABLE
```

We run [Snyk scans](https://app.snyk.io/).

## Project status
Empowered by Life.

## Contributing
This project welcomes contributions.

## License
Unlicensed. Or Universal Licensed. Whatever. This is in our playground.